
Copyright 2005 http://2bits.com

This module provides the ability for site admins to track clicks
on their site such as advertising campaigns and newsletter click
throughs.

The clicks are organized into groups, so ad campaigns can be 
tracked if they involve more than one node.

Basic Functionality
-------------------
The module works by displaying a special link (such as click/123/2)
on each node. This is only visible to the admin or other users who
have special privileges.

This contains the node id and the group id.

The admin then uses this link externally (e.g. for advertising or
newsletters). When visitors click on that link, the database is
updated with daily, weekly and total clicks.

Each node will have an extra tab that displays detailed clicks.
There is also an overall click report under admin/logs.

Installation
------------
To install this module, do the following:

1. Upload the click directory and all its contents to your modules directory.

Configuration
-------------
To enable this module do the following:

1. Go to Admin -> Site building -> Modules, and enable click.

2. Go to Admin -> User management -> Access Control and enable viewing for the roles you want.

3. Go to Admin -> Site Configuration -> click.

   Select which group you want to be active.
   Note that this just changes the "click link" that is displayed on each
   node to the admin. Click groups are active for visitors at all times.

Bugs/Features/Patches:
----------------------
If you want to report bugs, feature requests, or submit a patch, please do so
at the project page on the Drupal web site.

Author
------
Khalid Baheyeldin (http://baheyeldin.com/khalid and http://2bits.com)

If you use this module, find it useful, and want to send the author
a thank you note, then use the Feedback/Contact page at the URL above.

The author can also be contacted for paid customizations of this
and other modules.
